<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @link          http://seldszar.fr Seldszar.fr
 * @package       Twig.Config
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Register related Twig paths
 */
	App::build(array(
		'Error' => array('%s' . 'Error' . DS),
		'View/Extension' => array('%s' . 'View' . DS . 'Extension' . DS)
	), App::REGISTER);

/**
 * Import related Twig files
 */
	App::import('Twig.Vendor', 'Twig/lib/Twig/Autoloader');
	App::import('Twig.Error', 'exceptions');

/**
 * Load Twig library
 */
	Twig_Autoloader::register();
