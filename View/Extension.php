<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @link          http://seldszar.fr Seldszar.fr
 * @package       Twig.View
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('ObjectCollection', 'Utility');

/**
 * A twig extension
 *
 * @package       Twig.View
 * @autor         Alexandre Breteau
 */
class Extension extends Twig_Extension {

/**
 * Extension name
 *
 * @var string
 */
	public $name = null;

/**
 * Settings for this extension
 *
 * @var array
 */
	public $settings = array();
/**
 * Extension filters
 *
 * @var array
 */
	public $filters = array();

/**
 * Extension tests
 *
 * @var array
 */
	public $tests = array();

/**
 * Extension functions
 *
 * @var array
 */
	public $functions = array();

/**
 * Extension globals
 *
 * @var array
 */
	public $globals = array();

/**
 * The View instance this extension is attached to
 *
 * @var View
 */
	protected $_View;

/**
 * Constructor
 */
	public function __construct(View $View, $settings = array()) {
		$this->_View = $View;
		if ($settings) {
			$this->settings = Hash::merge($this->settings, $settings);
		}
		if ($this->name === null) {
			$this->name = substr(get_class($this), 0, -9);
		}
	}

/**
 * Called after extension load
 */
	public function afterLoad() {
	}

/**
 * Called at startup
 *
 * @param Twig_Environment $environment Twig environment
 */
	public function initRuntime(Twig_Environment $environment) {
		$this->afterLoad();
	}

/**
 * Return extension filters
 *
 * @return array
 */
	public function getFilters() {
		$filters = ObjectCollection::normalizeObjectArray($this->filters);
		return array_map(function($properties) {
			return new Twig_SimpleFilter($properties['class'], array($this, $properties['class'] . 'Filter'), $properties['settings']);
		}, $filters);
	}

/**
 * Return extension tests
 *
 * @return array
 */
	public function getTests() {
		$tests = ObjectCollection::normalizeObjectArray($this->tests);
		return array_map(function($properties) {
			return new Twig_SimpleTest($properties['class'], array($this, $properties['class'] . 'Test'), $properties['settings']);
		}, $tests);
	}

/**
 * Return extension functions
 *
 * @return array
 */
	public function getFunctions() {
		$functions = ObjectCollection::normalizeObjectArray($this->functions);
		return array_map(function($properties) {
			return new Twig_SimpleFunction($properties['class'], array($this, $properties['class'] . 'Function'), $properties['settings']);
		}, $functions);
	}

/**
 * Return extension globals
 *
 * @return array
 */
	public function getGlobals() {
		return $this->globals;
	}

/**
 * Return twig extension name
 *
 * @return string
 */
	public function getName() {
		return $this->name;
	}

}
