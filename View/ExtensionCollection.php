<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @link          http://seldszar.fr Seldszar.fr
 * @package       Twig.View
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('ObjectCollection', 'Utility');

/**
 * A collection of Extensions objects
 *
 * @package       Twig.View
 */
class ExtensionCollection extends ObjectCollection {

/**
 * View object to use when making helpers.
 *
 * @var View
 */
	protected $_View;

/**
 * Constructor
 *
 * @param View $view
 */
	public function __construct(View $view) {
		$this->_View = $view;
	}

	public function load($extension, $settings = array()) {
		if (is_array($settings) && isset($settings['className'])) {
			$alias = $extension;
			$helper = $settings['className'];
		}
		list($plugin, $name) = pluginSplit($extension, true);
		if (!isset($alias)) {
			$alias = $name;
		}

		if (isset($this->_loaded[$alias])) {
			return $this->_loaded[$alias];
		}
		$extensionClass = $name . 'Extension';
		App::uses($extensionClass, $plugin . 'View/Extension');
		if (!class_exists($extensionClass)) {
			throw new MissingExtensionException(array(
				'class' => $extensionClass,
				'plugin' => substr($plugin, 0, -1)
			));
		}
		$this->_loaded[$alias] = new $extensionClass($this->_View, $settings);
		$enable = isset($settings['enabled']) ? $settings['enabled'] : true;
		if ($enable) {
			$this->enable($alias);
		}
		return $this->_loaded[$alias];
	}

}
