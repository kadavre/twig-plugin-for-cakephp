<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @link          http://seldszar.fr Seldszar.fr
 * @package       Twig.View
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('View', 'View');
App::uses('ExtensionCollection', 'Twig.View');

/**
 * Twig template engine adapter for CakePHP
 *
 * @package       Twig.View
 * @autor         Alexandre Breteau
 */
class TwigView extends View {

/**
 * Default view file extension
 *
 * @var string
 * @see View::ext
 */
	public $ext = '.twig';

/**
 * Indicate if the view layout is included
 *
 * @var boolean
 * @see View::autoLayout
 */
	public $autoLayout = false;

/**
 * Extensions used
 *
 * @var ExtensionCollection
 */
	public $Extensions;

/**
 * Extensions used
 *
 * @var array
 */
	public $extensions = array('Twig.Cake');

/**
 * Allow hybrid parsing (PHP and Twig)
 *
 * @var boolean
 */
	public $hybridParsing = true;

/**
 * Twig chain loader
 *
 * @var Twig_Loader_Chain
 */
	protected $ChainLoader;

/**
 * Twig filesystem loader
 *
 * @var Twig_Loader_Filesystem
 */
	protected $FilesystemLoader;

/**
 * Twig environment
 *
 * @var Twig_Environment
 */
	protected $Environment;

/**
 * Indicate that paths have been loaded
 *
 * @var boolean
 */
	protected $_pathsLoaded = false;

/**
 * Indicate that extensions have been loaded
 *
 * @var boolean
 */
	protected $_extensionsLoaded = false;

/**
 * Constructor
 *
 * @param Controller $controller 
 */
	public function __construct(Controller $controller = null) {
		$this->_passedVars = Hash::merge(array('extensions', 'hybridParsing'), $this->_passedVars);
		parent::__construct($controller);
		if ($this->extensions === null) {
			$this->extensions = array();
		}
		if ($this->hybridParsing === null) {
			$this->hybridParsing = true;
		}
		$this->Extensions = new ExtensionCollection($this);
		$this->FilesystemLoader = new Twig_Loader_Filesystem(App::path('View'));
		$this->ChainLoader = new Twig_Loader_Chain(array($this->FilesystemLoader, new Twig_Loader_String())); 
		$this->Environment = new Twig_Environment($this->ChainLoader, array(
			'cache' => CACHE . 'views',
			'autoescape' => false,
			'debug' => Configure::read('debug') > 0,
		));
	}

/**
 * Load all available extensions
 */
	public function loadExtensions() {
		$extensions = ExtensionCollection::normalizeObjectArray($this->extensions);
		foreach ($extensions as $properties) {
			list(, $class) = pluginSplit($properties['class']);
			$this->Environment->addExtension($this->Extensions->load($properties['class'], $properties['settings']));
		}
		$this->_extensionsLoaded = true;
	}

/**
 * Load paths into Twig loader
 *
 * @param mixed $paths Additional paths to include
 */
	public function loadPaths($paths = array()) {
		if (!is_array($paths)) {
			$paths = array($paths);
		}
		$plugins = App::objects('plugin');
		foreach ($plugins as $plugin) {
			$this->addPaths($this->_paths($plugin), $plugin);
		}
		$this->addPaths($this->_paths());
		$this->addPaths($paths);
		$this->_pathsLoaded = true;
	}

/**
 * Add a view path
 *
 * @param string $path
 * @param string $plugin
 */
	public function addPath($path, $plugin = null) {
		if (file_exists($path)) {
			$namespace = $plugin === null ? '__main__' : $plugin;
			$this->FilesystemLoader->addPath($path, $namespace);
		}
	}

/**
 * Add some view paths
 *
 * @param array $paths
 * @param string $plugin
 */
	public function addPaths($paths, $plugin = null) {
		foreach ($paths as $path) {
			$this->addPath($path, $plugin);
		}
	}

/**
 * Renders view for given view file and layout
 *
 * @param string $view 
 * @param string $layout 
 * @return string 
 * @see View::render()
 */
	public function render($view = null, $layout = null) {
		if ($this->hasRendered) {
			return true;
		}
		if (!$this->_pathsLoaded) {
			$viewFile = $this->_getViewFileName($view);
			$this->loadPaths(dirname($viewFile));
		}
		if (!$this->_extensionsLoaded) {
			$this->loadExtensions();
		}
		return parent::render($view, $layout);
	}

/**
 * Evaluate Twig view
 *
 * @param string $viewFilePath 
 * @param array $dataForView 
 * @return string Rendered view
 * @see View::_evaluate()
 */
	protected function _evaluate($viewFile, $dataForView) {
		try {
			$render = basename($viewFile);
			if ($this->hybridParsing) {
				$render = parent::_evaluate($viewFile, $dataForView);
			}
			return $this->Environment->render($render, $dataForView);
		} catch (Twig_Error $e) {
			throw new TwigErrorException($e->getRawMessage());
		}
	}

/**
 * Return available view extensions
 *
 * @return array 
 * @see View::_getExtensions()
 */
	protected function _getExtensions() {
		$exts = array($this->ext);
		if ($this->ext !== '.twig') {
			$exts[] = '.twig';
		}
		return $exts;
	}

}
