<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @link          http://seldszar.fr Seldszar.fr
 * @package       Twig.View.Extension
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Extension', 'Twig.View');

/**
 * Cake extension
 *
 * @package       Twig.View.Extension
 * @autor         Alexandre Breteau
 */
class CakeExtension extends Extension {

/**
 * Called after extension load
 *
 * @see Extension::afterLoad()
 */
	public function afterLoad() {
		$this->addHelpers();
	}

/**
 * Add view helpers to Twig globals
 */
	protected function addHelpers() {
		$helpers = HelperCollection::normalizeObjectArray($this->_View->helpers);
		foreach ($helpers as $properties) {
			list(, $class) = pluginSplit($properties['class']);
			$this->globals[$class] = $this->_View->Helpers->load($properties['class'], $properties['settings']);
		}
	}

}
