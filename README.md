# Twig Plugin for CakePHP #

## Setup ##

### Getting plugin ###

Clone source code from this repository into your `Plugin/Twig` directory.

### Load plugin ###

Add these lines in `Config/bootstrap.php` :

	Cake::loadPlugin('Twig' => array(
		'bootstrap' => true
	));

## Twig View ##

### How to use ###

This view allow to use Twig engine, to activate this view, add this line in your model :

	public $viewClass = 'Twig.Twig';

## Twig Extension ##

### How to use ###

Extensions allow to create methods to use in Twig views, you can add with `$extensions` property.
This property contains extensions name, same as `$helpers`

	App::uses('AppController', 'Controller');

	class MyController extends AppController {

		public $viewClass = 'Twig.Twig';

		public $extensions = array();

	}

### Create an extension ###

You can create custom extensions in `View/Extension` folder.

This is an extension example :

	App::uses('Extension', 'Twig.View');

	class MyExtension extends Extension {
	}

### Integrated extensions ###

At the moment you can use these following extenions :

 * `CakeExtension` adds helpers of the controller

## Branch strategy ##

This repository is splitted into following main branches :

 * `master` contains releases and stable revisions
 * `develop` contains new features
